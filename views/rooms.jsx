"use strict";

const React = require("react");

const Layout = require("./layout");

function Room({ room }) {
	return (
		<div className="room">
			<div className="name">{room.name}</div>
			<div className="userCount">{room.num_joined_members} users</div>
			<div className="alias">{room.canonical_alias}</div>
			<div className="topic">{room.topic}</div>
		</div>
	);
}

module.exports = function Rooms({ rooms, nextBatchUrl, previousBatchUrl }) {
	return (
		<Layout>
			<div className="navigation">
				{(previousBatchUrl != null)
					? <a href={previousBatchUrl}>&lt;&lt; Previous</a>
					: null
				}
				{(previousBatchUrl != null && nextBatchUrl != null)
					? " | "
					: null
				}
				{(nextBatchUrl != null)
					? <a href={nextBatchUrl}>Next &gt;&gt;</a>
					: null
				}
			</div>
			{rooms.map((room) => {
				return <Room key={room.id} room={room} />;
			})}
		</Layout>
	);
};
