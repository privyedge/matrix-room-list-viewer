"use strict";

const React = require("react");

const Layout = require("./layout");

function Field({ id, type, name, label, children }) {
	return (
		<div className="formField">
			<label htmlFor={`field_${id}`}>{label}</label>&nbsp;
			<input type={type} name={name} id={`field_${id}`} />
		</div>
	);
}

module.exports = function Index() {
	return (
		<Layout>
			<form method="get" action="/show-rooms">
				<Field label="Homeserver" type="text" name="hostname" id="homeserver" />
				<button type="submit">Show rooms</button>
			</form>
		</Layout>
	);
};
